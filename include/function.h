//
// Created by alex on 8/27/18.
//

#ifndef FUNCTION_R_FUNCTION_H
#define FUNCTION_R_FUNCTION_H

#include <memory>
#include <utility>
#include <variant>
#include <algorithm>
#include <functional>

#include "callable.h"

#include <iostream>

constexpr int SMALL_ALLOCATION_OPTIMIZATION_THRESHOLD = 24;

template<class ...>
class function;

template<class Result, class ...Args>
Result null_functor(Args...);

template<class Result, class ...Args>
class function<Result(Args...)>
{
    template<class T>
    class CallableT: public Callable<T, Result, Args...>
    {
    public:
        explicit CallableT(const T &functor) : Callable<T, Result, Args...>(functor) {};
    };
public:

    function()
    {
        SetCallable(null_functor<Result, Args...>);
    }

    function(std::nullptr_t)
    {
        SetCallable(null_functor<Result, Args...>);
    }

    function(const function &other)
    {
        CopyFromCallable(other);
    }

    function(function &&other) noexcept
    {
        m_callable = other.m_callable;
        other.alloc(null_functor<Result, Args...>);
    }

    template<typename T>
    function(T t)
    {
        SetCallable<T>(std::forward<T>(t));
    }

    ~function()
    {
        ClearCallable();
    }

    template<class F>
    function& operator=(F&& f)
    {

        ClearCallable();
        SetCallable<F>(std::forward<F>(f));

        return *this;

    }


    function& operator=(const function& other)
    {
        ClearCallable();
        CopyFromCallable(other);

        return *this;
    }

    function& operator=(std::nullptr_t)
    {
        ClearCallable();
        m_callable = new CallableT<decltype(&null_functor<Result, Args...>)>(null_functor<Result, Args...>);

        return *this;

    }

    function& operator=(function&& other)
    {
        ClearCallable();
        m_callable = other.m_callable;
        other.alloc(null_functor<Result, Args...>);

        return *this;
    }

    template<class F>
    function& operator=( std::reference_wrapper<F> f )
    {
        ClearCallable();
        SetCallable(std::forward(f.get()));

        return *this;
    }

    Result operator()(Args ...args)
    {
        return CallCallable(std::forward<Args>(args)...);
    }

    void swap( function& other ) noexcept
    {
        std::swap(m_callable, other.m_callable);
    }
private:
    /**Ugly Low Level Code**/

    template <class T>
    void SetCallable(T &&t)
    {
        if(sizeof(T) <= SMALL_ALLOCATION_OPTIMIZATION_THRESHOLD)
        {
            std::array<char, SMALL_ALLOCATION_OPTIMIZATION_THRESHOLD> mem{};

            new(&mem[0]) CallableT<T>(std::forward<T>(t));
            m_callable = mem;
        }
        else
            m_callable = new CallableT<T>(std::forward<T>(t));
    }

    void ClearCallable()
    {
        using T = CallableBase<Result, Args...>;

        if(std::holds_alternative<T*>(m_callable))
            delete std::get<0>(m_callable);
        else
            reinterpret_cast<T*>(&std::get<1>(m_callable)[0])->~T();
    }

    void CopyFromCallable(const function &other)
    {
        using T = CallableBase<Result, Args...>;

        if(std::holds_alternative<T*>(other.m_callable))
        {
            auto otherCallable = std::get<0>(other.m_callable);

            auto p = (void*) new char[otherCallable->GetSize()];
            otherCallable->CopyTo(p);
            m_callable = static_cast<T*>(p);
        }
        else
            m_callable = std::get<1>(other.m_callable);

    }

    Result CallCallable(Args ...args)
    {
        using T = CallableBase<Result, Args...>;
        T* callable;

        if(std::holds_alternative<T*>(m_callable))
            callable = std::get<0>(m_callable);
        else
            callable = reinterpret_cast<T*>(&std::get<1>(m_callable)[0]);

        return (*callable)(std::forward<Args>(args)...);
    }

private:
    std::variant<CallableBase<Result, Args...>*, std::array<char, SMALL_ALLOCATION_OPTIMIZATION_THRESHOLD>> m_callable;
};


template<class Result, class ...Args>
Result null_functor(Args...)
{
    throw std::bad_function_call(); // TODO: replace this
}

#endif //FUNCTION_R_FUNCTION_H
