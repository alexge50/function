//
// Created by alex on 8/27/18.
//

#ifndef FUNCTION_R_CALLABLE_H
#define FUNCTION_R_CALLABLE_H

#include <stdint.h>
#include <utility>

template<class Result, class ...Args>
class CallableBase
{
public:
    virtual ~CallableBase() = default;
    virtual Result operator()(Args ...args) = 0;

    virtual size_t GetSize() = 0;
    virtual void CopyTo(void*) = 0;
};

template<class Functor, class Result, class ...Args>
class Callable: public CallableBase<Result, Args...>
{
public:
    explicit Callable(const Functor &functor): m_functor(functor) {};
    ~Callable() override = default;

    Result operator()(Args ...args) override { return m_functor(std::forward<Args>(args)...); }

    size_t GetSize() override
    {
        return sizeof(Callable);
    }

    void CopyTo(void *p) override
    {
        new(p) Callable(m_functor);
    }

private:
    Functor m_functor;
};

#endif //FUNCTION_R_CALLABLE_H
