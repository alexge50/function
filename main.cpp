#include <iostream>

#include "include/function.h"

#include <functional>

struct Functor
{
public:
    int operator()()
    {
        std::cout << "World" << std::endl;
    }
};

int main()
{
    int i;
    function<int()> f = [i=0] () mutable { return i++; };
    //std::function<int()> f = [i=0] () mutable { return i++; };

    //f = [i=0] () mutable { return i++; };

    auto a = [i=0] () mutable { return i++; };

    std::cout << f() << std::endl;


    return 0;
}